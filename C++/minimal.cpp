#include <cstdio>
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"
#include "tensorflow/lite/optional_debug_tools.h"
#include <iostream>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#define USE_UINT8

using namespace tflite;
using namespace std;
using TfLiteDelegatePtr = tflite::Interpreter::TfLiteDelegatePtr;
using TfLiteDelegatePtrMap = std::map<std::string, TfLiteDelegatePtr>;

void show_model_info(std::unique_ptr<Interpreter> &interpreter)
{
    vector<int> inputs = interpreter->inputs();
    vector<int> outputs = interpreter->outputs();

    cout << "==========Model info==========" << endl;
    for(int idx = 0; idx < inputs.size(); idx ++){
        TfLiteIntArray* dims = interpreter->tensor(inputs[idx])->dims;
        cout << interpreter->GetInputName(idx) << ":[" ;
        for(int iidx = 0; iidx < dims->size; iidx ++){
            if(iidx == 0)
                cout << dims->data[iidx] ;
            else
                cout << ", " << dims->data[iidx] ;
        }
        cout << "]  ";
        switch(interpreter->tensor(inputs[idx])->type){
            case kTfLiteFloat32:
                cout << "FLOAT" << endl;
                break;
            case kTfLiteUInt8:
                cout << "UINT" << endl;
                break;
            default:
                cout << "NO SUCH TYPE" << endl;
                exit(-1);
        }
    }
    for(int idx = 0; idx < outputs.size(); idx ++){
        TfLiteIntArray* dims = interpreter->tensor(outputs[idx])->dims;
        cout << interpreter->GetOutputName(idx) << ":[" ;
        for(int iidx = 0; iidx < dims->size; iidx ++){
            if(iidx == 0)
                cout << dims->data[iidx] ;
            else
                cout << ", " << dims->data[iidx] ;
        }
        cout << "]  ";
        switch(interpreter->tensor(outputs[idx])->type){
            case kTfLiteFloat32:
                cout << "FLOAT" << endl;
                break;
            case kTfLiteUInt8:
                cout << "UINT" << endl;
                break;
            default:
                cout << "NO SUCH TYPE" << endl;
                exit(-1);
        }
    }
    cout << "=========end==========" << endl << endl;

    //    tflite::PrintInterpreterState(interpreter.get());
}

int main(int argc, char* argv[]) {
#ifdef USE_UINT8
    string filename = "v3-large-minimalistic_224_1.0_uint8.tflite";
#else
    string filename = "v3-large-minimalistic_224_1.0_float.tflite";
#endif
    string image_path = "../cat.jpg";

    // Load model
    std::unique_ptr<tflite::FlatBufferModel> model = tflite::FlatBufferModel::BuildFromFile(filename.c_str());

    // Build the interpreter
    tflite::ops::builtin::BuiltinOpResolver resolver;
    InterpreterBuilder builder(*model, resolver);
    std::unique_ptr<Interpreter> interpreter;
    builder(&interpreter);

    vector<int> input_shape = {1, 224, 224, 3};
    interpreter->ResizeInputTensor(interpreter->inputs()[0], input_shape);
    interpreter->UseNNAPI(false);
    interpreter->SetAllowFp16PrecisionForFp32(false);
    interpreter->SetNumThreads(1);

    show_model_info(interpreter);

    // Allocate tensor buffers.
    cout << "allocate" << endl;
    if (interpreter->AllocateTensors() != kTfLiteOk) {
      cout << "Failed to allocate tensors!" << endl;
    }

    cv::Mat image;
    image = cv::imread(image_path, 1);
    cv::resize(image, image, cv::Size(224, 224));
#ifdef USE_UINT8
    image.convertTo(image, CV_8U);
    uint8_t* input = interpreter->typed_input_tensor<uint8_t>(0);
#else
    image.convertTo(image, CV_32F);
    image = image / 255.;

    float* input = interpreter->typed_input_tensor<float>(0);
#endif
    
    memcpy(input, image.data, image.total()* image.elemSize());

    // Run inference
    if (interpreter->Invoke() != kTfLiteOk) {
        cout << "Fail to inference" << endl;
    }

#ifdef USE_UINT8
    uint8_t* output = interpreter->typed_output_tensor<uint8_t>(0);
    cv::Mat output_mat(1, 1001, CV_8U);
    memcpy(output_mat.data, output, 1*1001*sizeof(uint8_t));
#else
    float* output = interpreter->typed_output_tensor<float>(0);
    cv::Mat output_mat(1, 1001, CV_32F);
    memcpy(output_mat.data, output, 1*1001*sizeof(float));
#endif
//    cout << output_mat << endl;
    cout << cv::sum(output_mat)[0] << endl;

    double min, max;
    cv::Point min_loc, max_loc;
    cv::minMaxLoc(output_mat, &min, &max, &min_loc, &max_loc);
    cout << max_loc << endl;
    cv::imshow("tmp", image);
    cv::waitKey(0);

    return 0;
}
