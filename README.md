# Basic TFLite tutorial

2021/06 Update to TF2.5

Some useful tutorial for using TFLite in Android arm.
If you find it useful for your project, please consider giving a like

# Build libtensorflow-lite from source
Tensorflow is officially built in bazel, but you may have your project built in cmake

That is, here are the steps for building `libtensorflowlite.so` so that you can link tflite library into your project via CMakeLists.txt

You can check my [CMakeList.txt](C++/CMakeLists.txt) as well!

## Build all in once
```
artifacts/
├── bin
│   ├── arm64
│   │   └── benchmark_model
│   └── x86
│       └── benchmark_model
├── lib
│   ├── arm64
│   │   └── libtensorflow.so
│   └── x86
│       └── libtensorflow.so
└── src

```


## Build in Local machine

1. Clone tensorflow
	```
	# git clone https://github.com/tensorflow/tensorflow.git
	```
2. Install bazel, remember to install the corresponding version of bazel to tensorflow. Otherwise, it may fail.
	* note: if bazel version is not corrected, it can also generate `libtensorflowlite.so` but in very small size. It is wrong!!
3. Run `./configure` under `tensorflow/` (You may need Android-sdk/ndk or something else)
4. You may need to run `tensorflow/lite/tools/make/download_dependencies.sh` first to collect dependencies
5. Then build `libtensorflowlite.so`, the output will be stored in `tensorflow-bin/
	```
	bazel build -c opt --crosstool_top=//external:android/crosstool --host_crosstool_top=@bazel_tools//tools/cpp:toolchain --cxxopt="-std=c++11" --fat_apk_cpu=arm64-v8a --config=android_arm64 //tensorflow/lite:libtensorflowlite.so
	``` 
6. Build `benchmark_model`:
   ```
   bazel build -c opt --config=android_arm64 //tensorflow/lite/tools/benchmark:benchmark_model
   ```
7. Copy include from tensorflow, the folder structure should be
	```
	- absl/
	- Eigen/
	- farmhash/
	- fft2d/
	- flatbuffers/
	- tensorflow/
		- lite/
			- c/
			- core/
			and others...
	```

## Build in Docker
1. Pull tensorflow from docker, only images with `devel` tag contain tensorflow source code
2. Build container
   ```
   docker run -it -w /tensorflow_src tensorflow/tensorflow:devel bash
   ```
3. Build tensorflow inside docker, note that `bazel`, `python` is already installed in docker, but you need to get `android_ndk` on your self
4. Other steps are just alike building in local machine



## Include Delegate in TFlite

### GPU Delegate
If GPU delegate is needed, add the following lines in `tensorflow/lite/BUILD`
```
"//tensorflow/lite/delegates/gpu:gl_delegate",
"//tensorflow/lite/delegates/gpu:delegate",
```

### Hexagon Delegate
If Hexagon delegate is needed, add the following lines in `tensorflow/lite/BUILD`
```
"//tensorflow/lite/delegates/hexagon:hexagon_delegate",
```

If Hexagon is needed, we need `libhexagon_interface.so` and `libhexagon_nn_skel*.so`

* `libhexagon_interface.so`: 
	```
	bazel build --config=android_arm64 tensorflow/lite/delegates/hexagon/hexagon_nn:libhexagon_interface.so

	place into /data/local/tmp(default path) for inference
	```
* `libhexagon_nn_skel*.so` can be downloaded [here](https://www.tensorflow.org/lite/performance/hexagon_delegate), check your corresponding skel version in `tensorflow/3rdparty/hexagon/workspace.bzl`

* For some machines, it is needed to type `export LD_LIBRARY_PATH=/vendor/lib64` to enable Hexagon delegate
	
### XNNPack Delegate
* To enable xnnpack on default, add `--define tflite_with_xnnpack=true` when build
* To enable quantized inference, add `--define xnn_enable_qs8=true` when build



## Corresponding Version Table
|       | Bazel verision | Android NDK Version | Hexagon | XNNPack |
| ----- | -------------- | ------------------- | ------- | ------- |
| TF2.5 | 3.7.2          |                     | O       | O       |

note: bazel version can be found in .bazelversion in tensorflow

## References

* [how-to-build-your-c-program](https://danny270degree.blogspot.com/2018/07/tensorflow-how-to-build-your-c-program.html)
* [tensorflow/lite/python/lite.py](https://github.com/tensorflow/tensorflow/blob/r1.14/tensorflow/lite/python/lite.py)
* [Tensorflow build from source](https://www.tensorflow.org/install/source?hl=zh-tw)
* [GPU](https://www.tensorflow.org/lite/performance/gpu#c-until-2.3.0)
* [Hexagon](https://www.tensorflow.org/lite/performance/hexagon_delegate)
* [XNNpack](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/delegates/xnnpack/README.md)
