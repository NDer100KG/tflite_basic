export SDK_VERSION=r25.2.5
export NDK_VERSION=r19c

if [ $# = 2 ]; then
  SDK_VERSION=$1
  NDK_VERSION=$2
fi

export ANDROID_DEV_HOME=/android
mkdir -p ${ANDROID_DEV_HOME}

export ANDROID_SDK_FILENAME=tools_${SDK_VERSION}-linux.zip
export ANDROID_SDK_URL=https://dl.google.com/android/repository/${ANDROID_SDK_FILENAME}
export ANDROID_API_LEVEL=23
export ANDROID_NDK_API_LEVEL=21

export ANDROID_BUILD_TOOLS_VERSION=28.0.0
export ANDROID_SDK_HOME=${ANDROID_DEV_HOME}/sdk
export PATH=${PATH}:${ANDROID_SDK_HOME}/tools:${ANDROID_SDK_HOME}/platform-tools

export ANDROID_NDK_FILENAME=android-ndk-${NDK_VERSION}-linux-x86_64.zip
export ANDROID_NDK_URL=https://dl.google.com/android/repository/${ANDROID_NDK_FILENAME}
export ANDROID_NDK_HOME=${ANDROID_DEV_HOME}/ndk
export PATH=${PATH}:${ANDROID_NDK_HOME}
