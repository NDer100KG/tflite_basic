# Run configure.
configs=(
'/usr/bin/python3'
'/usr/lib/python3/dist-packages'
'N'
'N'
'N'
'-march=native -Wno-sign-compare'
'y'
'/android/ndk'
'/android/sdk'
)
printf '%s\n' "${configs[@]}" | ./configure

# Configure Bazel.
source tensorflow/tools/ci_build/release/common.sh
install_bazelisk

### build libtensorflowlite.so
echo -e "\e[1;31m Start building libtensorflowlite.so on arm64 \e[0m"
bazel build -c opt --crosstool_top=//external:android/crosstool --host_crosstool_top=@bazel_tools//tools/cpp:toolchain --cxxopt="-std=c++11" --fat_apk_cpu=arm64-v8a --config=android_arm64 //tensorflow/lite:libtensorflowlite.so
mkdir -p artifacts/lib/arm64
cp bazel-bin/tensorflow/lite/libtensorflowlite.so artifacts/lib/arm64/libtensorflow.so
cp bazel-bin/tensorflow/lite/delegates/hexagon/hexagon_nn/libhexagon_interface.so artifacts/lib/arm64/libhexagon_interface.so

echo -e "\e[1;31m Start building libtensorflowlite.so on x86 \e[0m"
bazel build -c opt --cxxopt="-std=c++11" //tensorflow/lite:libtensorflowlite.so
mkdir -p artifacts/lib/x86
cp bazel-bin/tensorflow/lite/libtensorflowlite.so artifacts/lib/x86/libtensorflow.so

## build benchmark_model

echo -e "\e[1;31m Start building benchmark_model on arm64 \e[0m"
bazel build -c opt --config=android_arm64 \
    --define xnn_enable_qs8=true \
    --define ENABLE_TFLITE_XNNPACK_DEQUANTIZED_INT8_WEIGHTS=1 \
    --define TFLITE_ENABLE_HEXAGON=1 \
    //tensorflow/lite/tools/benchmark:benchmark_model
    # --define tflite_with_xnnpack=true \
mkdir -p artifacts/bin/arm64
cp bazel-bin/tensorflow/lite/tools/benchmark/benchmark_model artifacts/bin/arm64/benchmark_model

echo -e "\e[1;31m Start building benchmark_model on x86 \e[0m"
bazel build -c opt //tensorflow/lite/tools/benchmark:benchmark_model
mkdir -p artifacts/bin/x86
cp bazel-bin/tensorflow/lite/tools/benchmark/benchmark_model artifacts/bin/x86/benchmark_model

### copy all srcs to artifacts
mkdir -p artifacts/src/tensorflow/
cp -r tensorflow/lite artifacts/src/tensorflow/