import tensorflow as tf
import numpy as np
import os
os.environ['CUDA_VISIBLE_DEVICES']="0"
import argparse

argparser = argparse.ArgumentParser()
argparser.add_argument(
	'-c',
	'--checkpoints_folder',
    help='towards checkpoint folder you want to test')

if __name__ == '__main__':
	args = argparser.parse_args()
	using_model_path = args.checkpoints_folder
	checkpoint = tf.train.get_checkpoint_state(using_model_path)
	input_checkpoint = checkpoint.model_checkpoint_path

	clear_devices = True
	g = tf.Graph()
	
	with tf.Session(graph=g) as sess:
		saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=clear_devices)

		saver.restore(sess, input_checkpoint)

		run_meta = tf.RunMetadata()

		### Calculate FLOPs
		# opts = tf.profiler.ProfileOptionBuilder.float_operation()
		# flops = tf.profiler.profile(graph=g, run_meta=run_meta, cmd='op', options=opts)
		# opts = tf.profiler.ProfileOptionBuilder.trainable_variables_parameter()    
		# params = tf.profiler.profile(graph=g, run_meta=run_meta, cmd='op', options=opts)

		# print('FLOPS: ', flops.total_float_ops, ' OPTS: ', params.total_parameters)

		for n in tf.get_default_graph().as_graph_def().node:
			if n.name.find("save") == -1 and n.name.find("Momentum") == -1 and n.name.find("gradients") == -1 and n.name.find("Loss") == -1 and n.name.find("define_second_stage_train") == -1 and n.name.find("define_weight_decay") == -1:
				print(n.name)

		# var = [v for v in tf.trainable_variables() if v.name == 'global_head/vlad/clusters:0'][0]
		
		output_node_names = 'input,MobilenetV3/Predictions/Softmax'
		output_graph_def = \
			tf.graph_util.convert_variables_to_constants(\
														sess, \
														tf.get_default_graph().as_graph_def(), \
														output_node_names.split(',')) 

		output_graph = using_model_path + 'model.pb'
		with tf.gfile.GFile(output_graph, "wb") as f:
			f.write(output_graph_def.SerializeToString())
			print("%d ops in the fi-nal graph." % len(output_graph_def.node))

		# tf.train.write_graph(output_graph_def, using_model_path, 'FCHardNet_text.pb', as_text=True) 

		print('==========Info==========')
		print('using: ' + input_checkpoint)
