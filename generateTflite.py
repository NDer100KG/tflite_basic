import tensorflow as tf
import numpy as np
import os
import argparse

argparser = argparse.ArgumentParser()
argparser.add_argument(
	'-g',
	'--input_graph',
	default='v3-large-minimalistic_224_1.0_uint8.pb',	
    help='towards checkpoint folder you want to test')
argparser.add_argument(
	'-q',
	'--quantize',
	type=int,
	default=1, 
	help='quantize or not')

if __name__ == '__main__':
	args = argparser.parse_args()
	quantization = args.quantize

	clear_devices = True
	
	with tf.Session() as sess:
		input_graph = args.input_graph

		input_arrays = ['input']
		output_arrays = ['MobilenetV3/Predictions/Softmax']

		converter = tf.lite.TFLiteConverter.from_frozen_graph(input_graph, input_arrays, output_arrays)
		# converter.post_training_quantize = True
		if quantization:
			converter.inference_type = tf.uint8
			converter.quantized_input_stats = {input_arrays[0]: (127., 128.)}
			# converter.default_ranges_stats = (0, 255)
			converter.allow_custom_ops = True
		tflite_model = converter.convert()

		if quantization:
			open("model_quant.tflite", "wb").write(tflite_model)
		else:
			open("model.tflite", "wb").write(tflite_model)

		print('==========Info==========')
		print('using: ' + args.input_graph, 'quantize: ', quantization)
