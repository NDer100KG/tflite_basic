### Install Android SDK
cd ${ANDROID_DEV_HOME} && \
wget -q ${ANDROID_SDK_URL} && \
unzip -q ${ANDROID_SDK_FILENAME} -d android-sdk-linux && \
bash -c "ln -sv ${ANDROID_DEV_HOME}/android-sdk-* ${ANDROID_SDK_HOME}"
echo "Done install Android SDK"

# ### Install Android NDK
cd ${ANDROID_DEV_HOME} && \
wget -q ${ANDROID_NDK_URL} && \
unzip -q ${ANDROID_NDK_FILENAME} -d ${ANDROID_DEV_HOME} && \
bash -c "ln -sv ${ANDROID_DEV_HOME}/android-ndk-${NDK_VERSION} ${ANDROID_NDK_HOME}"

echo "Done install Android NDK"

### Update SDK
printf y | android update sdk --no-ui -a --filter tools,platform-tools,android-${ANDROID_API_LEVEL},build-tools-${ANDROID_BUILD_TOOLS_VERSION}
